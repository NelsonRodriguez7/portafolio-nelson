import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-body-section-2',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './body-section-2.component.html',
  styleUrl: './body-section-2.component.css'
})
export class BodySection2Component {

}
